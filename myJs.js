
function Position(x, y) {
    this.xPosition = x;
    this.yPosition = y;
    this.draw = function () {
    }
    this.remove = function () {
        if (((this.xPosition >= (MyObject.xPosition - 11) && this.xPosition <= (MyObject.xPosition + 13)) && (this.yPosition >= (MyObject.yPosition - 5) && this.yPosition <= (MyObject.yPosition + 5))))
            $("#" + this.xPosition + "_" + this.yPosition + "").remove();
    }
}
function Wall(x, y) {
    this.xPosition = x;
    this.yPosition = y;
    this.draw = function () {/*  src='images/wall_0" + (this.yPosition - MyObject.yPosition) + ".png' */
        $('#current').append("<img id='" + this.xPosition + "_" + this.yPosition + "'>")
        $("#" + this.xPosition + "_" + this.yPosition + "").css({ "width": "45px", "content": "url('images/wall_0.png')"/*  + (this.yPosition - MyObject.yPosition) + ".png' )"*/, "top": ((this.yPosition - (MyObject.yPosition - 5)) * yCell + 68), "left": ((this.xPosition - (MyObject.xPosition - 11)) * xCell + 143) })

    }
}
Wall.prototype = new Position();
function Beast(x, y) {
    this.xPosition = x;
    this.yPosition = y;
    this.speed = 100;
    this.timer;
    this.isTimer = false;
    this.draw = function () {

        $('#current').append("<img id='" + this.xPosition + "_" + this.yPosition + "' src='images/beast_0.png'>")
        $("#" + this.xPosition + "_" + this.yPosition + "").css({ "content": "url('images/beast_0.png')", "top": ((this.yPosition - (MyObject.yPosition - 5)) * yCell + 68), "left": ((this.xPosition - (MyObject.xPosition - 11)) * xCell + 143) })
        /* clearInterval(this.timer); */
        this.followPlayer();
    }
    this.move = function (_x, _y) {
        if (!(Game.objects[this.xPosition + _x][this.yPosition + _y] == MyObject)) {
            if (!((Game.objects[this.xPosition + _x][this.yPosition + _y] instanceof Wall) || (Game.objects[this.xPosition + _x][this.yPosition + _y] instanceof Beast))) {
                var temp = Game.objects[this.xPosition + _x][this.yPosition + _y]
                temp.xPosition = this.xPosition;
                temp.yPosition = this.yPosition;
                Game.objects[this.xPosition + _x][this.yPosition + _y] = Game.objects[this.xPosition][this.yPosition];
                Game.objects[this.xPosition][this.yPosition] = temp;
                this.remove()
                this.xPosition += _x;
                this.yPosition += _y;
                this.isTimer = false;
                this.draw();

            }
            
            /* MyObject.moveStep(0, 0); */
        }
        else {
            
            this.isTimer = false;
            //looser
        }
        
    }

    this.followPlayer = function () {
        var _this = this;
        /* this.timer= setTimeout(this.wherePlayer.bind(this), this.speed); */
        if (!_this.isTimer) {
            _this.isTimer = true;
            setTimeout(function () {
                _this.wherePlayer();
            }, this.speed);
        }

    }
}
Beast.prototype = new Position();
Beast.prototype.wherePlayer = function () {
    if (MyObject.xPosition > this.xPosition)
        this.move(1, 0)
    else if (MyObject.xPosition < this.xPosition)
        this.move(-1, 0)
    else if (MyObject.yPosition > this.yPosition)
        this.move(0, 1)
    else if (MyObject.yPosition < this.yPosition)
        this.move(0, -1)

};

$('html').keydown(function (e) {
    var x = 0, y = 0;
    if (e.which == 40)
        y++
    else if (e.which == 38)
        y--
    else if (e.which == 37)
        x--
    else if (e.which == 39)
        x++
    MyObject.moveStep(x, y);


})

$(document).ready(function () {
    MyObject.moveStep(0, 0);
})










//............................................................................
// draw map here (levels)
var map;
function mapToObject(map) {
    var x, y, dictionary = {
        //wall
        'W': function () {
            return new Wall(x, y)
        },//space(can walk)
        '.': function () {
            return new Position(x, y)
        },//beast
        'B': function () {
            return new Beast(x, y)
        },//win(objective)
        'X': function () {
            return new Position(x, y)
        },//person
        '@': function () {
            MyObject.xPosition = x;
            MyObject.yPosition = y;
            return MyObject;
        }
    };
    for (y = 0; y < map.length; y += 1) {
        for (x = 0; x < map[y].length; x += 1) {
            Game.objects[x][y] = dictionary[map[y][x]]();
        }
    }
}

map = [
    '.......................................................................',
    '.......................................................................',
    '.......................................................................',
    '.......................................................................',
    '..........WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW...........',
    '..........W................................................W...........',
    '..........W.WWWWWW..WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW.W...........',
    '..........W.W....W..WW.....................................W...........',
    '..........W.W....W............B................B...........W...........',
    '..........W.W...WWWW.......................................W...........',
    '..........W.............B.....W.WWWW.......................W...........',
    '..........W...................W..@.W.......................W...........',
    '..........W...................WWWWWW............B..........W...........',
    '..........W...............B................................W...........',
    '..........W...........B....................................W...........',
    '..........W.WWWWWWWWWWWWWWWW....WWWWWWWWWWWWWWWWWWWWWWWWWW.W...........',
    '..........W................................................W...........',
    '..........W.WWWWWWWWWWWWWWWW....WWWWWWWWWWWWWWWWWWWWWWWWWW.W...........',
    '..........W...............................................XW...........',
    '..........WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW...........',
    '.......................................................................',
    '.......................................................................',
    '.......................................................................',
    '.......................................................................'
];
mapToObject(map);



    // step counter ---- best way var
    //finish
    // 3 try
    // draw small map - beast
    // beast speed (Level ++)
    //